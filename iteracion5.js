// Iteración #5: Calcular promedio de strings

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];

function averageWord(listString2){
    let sumDeStrings = 0;

    for (var i=0; i < listString2.length; i++) {
        if (typeof listString2[i] === 'number'){
            sumDeStrings += listString2[i];
        } else if (typeof listString2[i] === 'string') {
            sumDeStrings += listString2[i].length;
        } 
    }
    var numItems = listString2.length;
    return console.log(sumDeStrings/numItems);
}

averageWord(mixedElements)