// Iteration #8: Contador de repeticiones

const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];
  function repeatCounter(listWords) {
    var repets = {};

    for ( var i = 0; i < listWords.length; i++){
        if (listWords[i] in repets){
            repets[listWords[i]]++;
        } else {
            repets[listWords[i]] = 1;
        }
    } return console.log(repets);
  }
  repeatCounter(counterWords)
